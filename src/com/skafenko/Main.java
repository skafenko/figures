package com.skafenko;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Figure> figures = new ArrayList<>();
        figures.add(new Circle("красный", 20));
        figures.add(new Square("чорный", 20));
        figures.add(new Triangle("синий", 10, 20));
        figures.add(new Trapeze("зеленый", 20, 40, 4));

        for (Figure figure : figures) {
            System.out.println(figure);
        }
    }
}
