package com.skafenko;

public class Trapeze extends Figure {
    private int baseDown, baseUp, height;

    public Trapeze(String color, int baseDown, int baseUp, int height) {
        super(color);
        this.baseDown = baseDown;
        this.baseUp = baseUp;
        this.height = height;
    }

    public int getBaseDown() {
        return baseDown;
    }

    public int getBaseUp() {
        return baseUp;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public double getArea() {
        return (baseDown + baseUp) * height / 2;
    }

    @Override
    public String uniqueMethod() {
        return "длина основ: " + getBaseDown() + " и " + getBaseUp() + " ед.";
    }

    @Override
    public String draw() {
        return "трапеція";
    }
}
