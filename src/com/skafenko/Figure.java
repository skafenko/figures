package com.skafenko;

public abstract class Figure implements Drawable {
    private String color;

    public Figure(String color) {
        this.color = color;
    }

    public abstract double getArea();

    public String getColor() {
        return color;
    }

    public abstract String uniqueMethod();

    @Override
    public String toString() {
        return String.format("Фигура: %s, площадь: %.2f кв. ед., цвет: %s, %s", draw(), getArea(), getColor(), uniqueMethod());
    }
}
