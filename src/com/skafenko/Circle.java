package com.skafenko;

public class Circle extends Figure {
    private int radius;

    public Circle(String color, int radius) {
        super(color);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public String uniqueMethod() {
        return "радиус: " + getRadius() + " ед.";
    }

    @Override
    public String draw() {
        return "круг";
    }

}
