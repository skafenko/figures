package com.skafenko;

public class Triangle extends Figure {
    private int width, height;

    public Triangle(String color, int width, int height) {
        super(color);
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public double getArea() {
        return (width * height) / 2;
    }

    @Override
    public String uniqueMethod() {
        return "длина сторони и высоти, опущенои на ету сторону: " + getWidth() + " и " + getHeight() + " ед.";
    }

    @Override
    public String draw() {
        return "треугольник";
    }
}
