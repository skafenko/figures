package com.skafenko;

public class Square extends Figure {
    private int width;

    public Square(String color, int width) {
        super(color);
        this.width = width;
    }

    @Override
    public String draw() {
        return "квадрат";
    }

    public int getWidth() {
        return width;
    }

    @Override
    public double getArea() {
        return Math.pow(width, 2);
    }

    @Override
    public String uniqueMethod() {
        return "длина стороны: " + getWidth() + " ед.";
    }
}
