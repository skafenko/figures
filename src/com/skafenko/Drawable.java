package com.skafenko;

public interface Drawable {
    String draw();
}
